# Racket control

![Logo von OpenTT – Schläger mit Katze](assets/icon.png "Logo von OpenTT – Schläger mit Katze"){ align=right width=100 }

!!! tip ""
	Racket control without the mathematical hassle!

This is a small web app for easing the use of racket control tests in table tennis.

- [issues](https://gitlab.com/opentt/racket-control/-/issues)
- [changelog](https://gitlab.com/opentt/racket-control/-/blob/main/changelog.md)

This is no official app.
See the racket control guidelines here:

- [ITTF](https://www.ittf.com/committees/umpires-referees/documents/)
- [DTTB](https://www.tischtennis.de/dttb/regeln-satzung/satzung-ordnungen.html)


## License

This script is distributed under the terms of the GNU General Public License.

See [readme](https://gitlab.com/opentt/racket-control/-/blob/main/README.md?ref_type=heads) or [COPYING](https://gitlab.com/opentt/racket-control/-/blob/main/COPYING) for details.
