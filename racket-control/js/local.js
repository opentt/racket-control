// factor for float operations to work properly
// error is as follows: this delivers: 3.9499999999999997
// console.log(3.9 + 0.05)
const CORRECTION_FACTOR = 100000
function addCorrectedFloat(num1, num2) {
	return ((num1 * CORRECTION_FACTOR) + (num2 * CORRECTION_FACTOR)) / CORRECTION_FACTOR;
}

const ID_INPUT = "in";
const ID_OUTPUT = "out";
const ID_TYPE_BOOL = "Chk";
const ID_TYPE_TEXT = "Text";

const ID_TEMPLATE = "template";
const ID_CONTAINER = "container";
const ID_CONTENT = "content";

const RUBBER_BLACK = `Black`;
const RUBBER_COLOR = `Color`;

const MAXIMUM = `Maximum`;
const MINIMUM = `Minimum`;


const TEMPLATE_RESULT = `result`;


const TEMPLATE_VISUAL = `visual`;
const ID_CHK_VISUAL = `${ID_INPUT}${ID_TYPE_BOOL}Visual`;


const TEMPLATE_LARC = `larc`;
const ID_CHK_LARC = `${ID_INPUT}${ID_TYPE_BOOL}LARC`;


const TEMPLATE_FLATNESS = `flatness`;
const TEMPLATE_FLATNESS_SINGLE = `${TEMPLATE_FLATNESS}-single`;
const TEMPLATE_FLATNESS_MINMAX = `${TEMPLATE_FLATNESS}-minmax`;
const FLATNESS_MEASUREMENTS = `flatnessMeasurements`;
const ID_FLATNESS_SINGLE_MEASURE = `${ID_INPUT}${ID_TYPE_TEXT}FlatnessMeasure`;
const ID_FLATNESS_COMPUTATION = `${ID_OUTPUT}${ID_TYPE_TEXT}Flatness`;
const BTN_ADD_FLATNESS_MEASURE = `btnAdd${FLATNESS_MEASUREMENTS}`;
const BTN_DELETE_FLATNESS_MEASURE = `btnDelete${ID_FLATNESS_SINGLE_MEASURE}`;

const FLATNESS_MIN_MEASUREMENT_COUNT = 2;
const FLATNESS_LIMIT_MAXIMUM = 0.20;
const FLATNESS_LIMIT_MINIMUM = -0.50;


const TEMPLATE_THICKNESS = `thickness`;
const TEMPLATE_THICKNESS_SINGLE = `${TEMPLATE_THICKNESS}-single`;
const TEMPLATE_THICKNESS_AVERAGE = `${TEMPLATE_THICKNESS}-average`;
const THICKNESS_MEASUREMENTS = `thicknessMeasurements`;
const ID_THICKNESS_SINGLE_MEASURE = `${ID_INPUT}${ID_TYPE_TEXT}ThicknessMeasure`;
const ID_THICKNESS_AVERAGE = `${ID_OUTPUT}${ID_TYPE_TEXT}ThicknessAverage`;
const ID_THICKNESS_FINAL = `${ID_OUTPUT}${ID_TYPE_TEXT}ThicknessFinal`;

const THICKNESS_MIN_MEASUREMENT_COUNT = 4;
const THICKNESS_LIMIT_MAXIMUM = 4.05;


// initialize forms and content
appendTemplate(TEMPLATE_VISUAL, {id: ID_CHK_VISUAL});

for (let color of [RUBBER_BLACK, RUBBER_COLOR]) {

	appendTemplate(TEMPLATE_LARC, {id: ID_CHK_LARC, color: color});

	appendTemplate(TEMPLATE_FLATNESS, {id: FLATNESS_MEASUREMENTS, color: color, mincount: FLATNESS_MIN_MEASUREMENT_COUNT});
	for (let i = 1; i <= FLATNESS_MIN_MEASUREMENT_COUNT; i++) {
		addFlatnessMeasurement(i, color);
	}
	$(`#${BTN_ADD_FLATNESS_MEASURE}${color}`).click(function () {
		newIndex = $(`#${TEMPLATE_FLATNESS_SINGLE}-${color}-${ID_CONTAINER} div`).get().length + 1;
		addFlatnessMeasurement(newIndex, color);
	});
	for (let minmax of [MAXIMUM, MINIMUM]) {
		appendTemplate(TEMPLATE_FLATNESS_MINMAX, {id: ID_FLATNESS_COMPUTATION, color: color, minmax: minmax}, color);
	}

	appendTemplate(TEMPLATE_THICKNESS, {id: THICKNESS_MEASUREMENTS, color: color, mincount: THICKNESS_MIN_MEASUREMENT_COUNT});
	for (let i = 1; i <= THICKNESS_MIN_MEASUREMENT_COUNT; i++) {
		appendTemplate(TEMPLATE_THICKNESS_SINGLE, {id: ID_THICKNESS_SINGLE_MEASURE, index: i, color: color}, color);
	}
	appendTemplate(TEMPLATE_THICKNESS_AVERAGE, {id: ID_THICKNESS_AVERAGE, idfinal: ID_THICKNESS_FINAL, color: color}, color);

}

// add flatness measurement
function addFlatnessMeasurement(index, color) {
	appendTemplate(TEMPLATE_FLATNESS_SINGLE, {id: ID_FLATNESS_SINGLE_MEASURE, index: index, color: color, mincount: FLATNESS_MIN_MEASUREMENT_COUNT}, color);
	$(`#${BTN_DELETE_FLATNESS_MEASURE}${color}${index}`).click(function () {
		$(this).parents(".input-group").remove();
		computeMinMax();
	});
}


// check racket validity
function checkValidity() {

	isValid = true;
	errors = [];

	// visual inspection
	if (!$(`#${ID_CHK_VISUAL}`).is(":checked")) {
		isValid = false;
		errors.push(enrichTemplate("error-visual", {}));
	}

	for (let color of [RUBBER_BLACK, RUBBER_COLOR]) {

		// LARC
		if (!$(`#${ID_CHK_LARC}${color}`).is(":checked")) {
			isValid = false;
			errors.push(enrichTemplate("error-larc", {color: color}));
		}

		// flatness
		inputValues = $(`#${TEMPLATE_FLATNESS_SINGLE}-${color}-${ID_CONTAINER} input`).map(
				function() {
					return parseFloat($(this).val());
				}
			).get();
		measureCount = 0;
		for (let inputValue of inputValues) {
			if (!isNaN(inputValue)) {
				measureCount++;
			}
		}
		if (measureCount < FLATNESS_MIN_MEASUREMENT_COUNT) {
			isValid = false;
			errors.push(enrichTemplate("error-nominmax", {color: color, count: measureCount, mincount: FLATNESS_MIN_MEASUREMENT_COUNT}));
		}
		for (let minmax of [MAXIMUM, MINIMUM]) {
			value = parseFloat($(`#${ID_FLATNESS_COMPUTATION}${color}${minmax}`).val());
			if (!isNaN(value)) {
				limit = (minmax == MAXIMUM) ? FLATNESS_LIMIT_MAXIMUM : FLATNESS_LIMIT_MINIMUM;
				if (
						((minmax == MAXIMUM) && (value > limit))
						||
						((minmax == MINIMUM) && (value < limit))
				) {
					isValid = false;
					errors.push(enrichTemplate("error-minmax", {color: color, minmax: minmax, value: value, limit: limit}));
				}
			}
		}

		// thickness
		inputValues = $(`#${TEMPLATE_THICKNESS_SINGLE}-${color}-${ID_CONTAINER} input`).map(
				function() {
					return parseFloat($(this).val());
				}
			).get();
		measureCount = 0;
		for (let inputValue of inputValues) {
			if (!isNaN(inputValue)) {
				measureCount++;
			}
		}
		if (measureCount < THICKNESS_MIN_MEASUREMENT_COUNT) {
			isValid = false;
			errors.push(enrichTemplate("error-nothickness", {color: color, count: measureCount, mincount: THICKNESS_MIN_MEASUREMENT_COUNT}));
		}
		value = parseFloat($(`#${ID_THICKNESS_FINAL}${color}`).val());
		if (!isNaN(value) && (value >= THICKNESS_LIMIT_MAXIMUM)) {
			isValid = false;
			errors.push(enrichTemplate("error-thickness", {color: color, value: value, limit: THICKNESS_LIMIT_MAXIMUM}));
		}

	}

	fillTemplate(TEMPLATE_RESULT, {valid: isValid, errors: errors});

}
checkValidity();


// compute min and max flatness
function computeMinMax() {

	for (let color of [RUBBER_BLACK, RUBBER_COLOR]) {
		inputValues = $(`#${TEMPLATE_FLATNESS_SINGLE}-${color}-${ID_CONTAINER} input`).map(
				function() {
					return parseFloat($(this).val());
				}
			).get()
		values = [];
		for (let inputValue of inputValues) {
			if (!isNaN(inputValue)) {
				values.push(inputValue);
			}
		}
		for (let minmax of [MAXIMUM, MINIMUM]) {
			$(`#${ID_FLATNESS_COMPUTATION}${color}${minmax}`).val(
					(values.length == 0) ? "" :
							((minmax == MAXIMUM) ? Math.max(...values) : Math.min(...values)));
		}
	}

}


// compute average thickness
function computeAverage() {

	for (let color of [RUBBER_BLACK, RUBBER_COLOR]) {
		inputValues = $(`#${TEMPLATE_THICKNESS_SINGLE}-${color}-${ID_CONTAINER} input`).map(
				function() {
					return parseFloat($(this).val());
				}
			).get()
		valueCount = 0;
		sum = 0.0;
		for (let inputValue of inputValues) {
			if (!isNaN(inputValue)) {
				sum = addCorrectedFloat(sum, inputValue);
				valueCount++;
			}
		}
		$(`#${ID_THICKNESS_AVERAGE}${color}`).val((sum == 0) ? "" : (sum/valueCount));

	}

	$(`#${ID_THICKNESS_FINAL}${RUBBER_BLACK}`).val(computeFinalThickness(RUBBER_BLACK, RUBBER_COLOR));
	$(`#${ID_THICKNESS_FINAL}${RUBBER_COLOR}`).val(computeFinalThickness(RUBBER_COLOR, RUBBER_BLACK));

}
function computeFinalThickness(thisSide, otherSide) {

	averageThickness = parseFloat($(`#${ID_THICKNESS_AVERAGE}${thisSide}`).val());

	if (isNaN(averageThickness)) {
		return "";
	}

	finalThickness = averageThickness

	if (averageThickness > 0) {

		maxFlatnessThis = parseFloat($(`#${ID_FLATNESS_COMPUTATION}${thisSide}${MAXIMUM}`).val());
		maxFlatnessOther = parseFloat($(`#${ID_FLATNESS_COMPUTATION}${otherSide}${MAXIMUM}`).val());
		sumFlatness = addCorrectedFloat(maxFlatnessThis, maxFlatnessOther);

		if (!isNaN(maxFlatnessThis) && (maxFlatnessThis > 0.0)) {

			if (isNaN(maxFlatnessOther) || (maxFlatnessOther >= 0.0)) {
				finalThickness = addCorrectedFloat(averageThickness, maxFlatnessThis);
			} else {
				if (sumFlatness > 0) {
					finalThickness = addCorrectedFloat(averageThickness, sumFlatness);
				}
			}

		}

	}

	return finalThickness;

}


// set triggers
$("body").on("change", `#${ID_CHK_VISUAL}`, function () {
	checkValidity();
});

for (let color of [RUBBER_BLACK, RUBBER_COLOR]) {

	$("body").on("change", `#${ID_CHK_LARC}${color}`, function () {
		checkValidity();
	});

	$('body').on('keyup', `#${TEMPLATE_FLATNESS_SINGLE}-${color}-${ID_CONTAINER} input`, function() {
		computeMinMax();
		computeAverage();
		checkValidity();
	});

	$('body').on('keyup', `#${TEMPLATE_THICKNESS_SINGLE}-${color}-${ID_CONTAINER} input`, function() {
		computeAverage();
		checkValidity();
	});

}
