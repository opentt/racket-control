// Returns if values are equal
Handlebars.registerHelper('isGreater', function(value1, value2) {
	return value1 > value2;
});

// Returns if values are equal
Handlebars.registerHelper('isEqual', function(value1, value2) {
  return value1 == value2;
});

// Returns if color is black
Handlebars.registerHelper('isBlack', function(color) {
  return color == RUBBER_BLACK;
});

/* ---------------
 * Some helper functions for handlebar templates.
 */

// Returns handlebar template with given id.
function getTemplate(id) {
	return Handlebars.compile($(`#${id}-${ID_TEMPLATE}`).html());
}

// Return filled template with data.
function enrichTemplate(id, data) {
	template = getTemplate(id);
	return template(data);
}

// Calls template with data to fill contnt with given id.
function fillTemplate(id, data) {
	$(`#${id}-${ID_CONTENT}`).html(enrichTemplate(id, data));
}

// Appends template to container.
function appendTemplate(id, data, ...ids) {
	containerId = id;
	for (addid of ids) {
		containerId += `-${addid}`
	}
	$(`#${containerId}-${ID_CONTAINER}`).append(enrichTemplate(id, data));
}
