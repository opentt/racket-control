# Open-TT: Racket Control

Racket Control is a small web app to ease racket controls.
This is **not** an official app of ITTF or DTTB or such.

- feature requests, problems etc.: <https://gitlab.com/opentt/racket-control/issues>
- changes: [changelog](changelog.md)


Racket Control ist eine kleine Webapp, um Schlägertests zu vereinfachen.
Die App ist **keine** offizielle App der ITTF, des DTTB oder so.

- Wünsche, Probleme etc.: <https://gitlab.com/opentt/racket-control/issues>
- Änderungen: [changelog](changelog.md)

## Open-TT

Racket Control is part of the project "Open-TT" which provides open documents and applications for table tennis:

- <https://gitlab.com/opentt>

Racket Control ist Teil des Projekts "Open-TT", das offene Dokumente, Programme und Apps rund um Tischtennis anbietet.


## Git-Repository

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>

This means, there is always a stable mainline, the `main` branch.
This branch ist always compileable and testable, both without errors.


## Legal stuff

License of the app: GNU General Public License.
See file [COPYING](COPYING).

Which means the app is free and open source

- you can use the program as you want, even commercially
- you can share the program as you like
- if you change the source code, you have to distribute it under the same license, and you have to provide the source code of the programs


### Copyright

Copyright 2019-2024 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See [COPYING](COPYING) for details.

This file is part of Open-TT: Racket Control.

Open-TT: Racket Control is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Open-TT: Racket Control is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Open-TT: Racket Control. If not, see <http://www.gnu.org/licenses/>.
