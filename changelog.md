# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].


## [Unreleased]


## [1.1.1] - 2024-03-09

### Added

- test values from ods file #11

### Fixed

- precision problem of final thickness computation with correction factor #15
- description of upload script parameter `-w`


## [1.1.0] - 2024-02-17

### Added

- first selenium tests #11

### Changed

- showing flatness and thickness errors even if not enough measurements were taken #12

### Fixed

- missing delete button for flatness measurements #13


## [1.0.0] - 2024-02-09

initial version #1

### Added

- website for racket control
- visual check
- LARC check
- flatness measurements
- thickness measurements



[Unreleased]: https://gitlab.com/opentt/racket-control/-/compare/1.1.1...HEAD
[1.1.1]: https://gitlab.com/opentt/racket-control/-/compare/1.1.0...1.1.1
[1.1.0]: https://gitlab.com/opentt/racket-control/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/opentt/racket-control/-/tags/1.0.0

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
