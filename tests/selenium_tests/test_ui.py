import unittest

from selenium.webdriver.common.by import By

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.chrome.options import Options


TEST_URL = "https://www.opentt.de/racket-control/"
# TEST_URL = "http://localhost:8000/"

class TestUI(unittest.TestCase):

	def setUp(self):

		options = Options()
		options.add_argument("--headless")
		options.add_argument("--no-sandbox")

		self.driver = WebDriver(options=options)
		self.driver.get(TEST_URL)
		self.driver.set_window_size(1920, 1000)

		self.vars = {}


	def tearDown(self):
		driver = self.driver

		driver.close()
		driver.quit()


	def test_title(self):
		driver = self.driver

		self.assertEqual(driver.title, "OpenTT – Racket Control")


	def test_texts(self):
		driver = self.driver

		self.assertEqual(driver.title, "OpenTT – Racket Control")


		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-heading").text, "Der Schläger wird beanstandet.")
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-error-0").text, "Die allgemeine Beschaffenheit ist fehlerhaft.")
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-error-1").text, "Der schwarze Belag ist nicht gültig (LARC-Prüfung).")
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-error-2").text, "Die Ebenheit des schwarzen Belags ist nicht ausreichend vermessen worden (0 Messungen statt mindestens 2 Messungen).")
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-error-3").text, "Die Dicke des schwarzen Belags ist nicht ausreichend vermessen worden (0 Messungen statt 4 Messungen).")
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-error-4").text, "Der farbige Belag ist nicht gültig (LARC-Prüfung).")
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-error-5").text, "Die Ebenheit des farbigen Belags ist nicht ausreichend vermessen worden (0 Messungen statt mindestens 2 Messungen).")
		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-error-6").text, "Die Dicke des farbigen Belags ist nicht ausreichend vermessen worden (0 Messungen statt 4 Messungen).")


# run all tests (funcations that start with "test_")
if __name__ == "__main__":

	unittest.main()

# eof
