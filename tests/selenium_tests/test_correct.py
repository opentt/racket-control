import unittest

from parameterized import parameterized

from selenium.webdriver.common.by import By

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.chrome.options import Options


TEST_URL = "https://www.opentt.de/racket-control/"
# TEST_URL = "http://localhost:8000/"

class TestUI(unittest.TestCase):

	def setUp(self):

		options = Options()
		options.add_argument("--headless")
		options.add_argument("--no-sandbox")

		self.driver = WebDriver(options=options)
		self.driver.get(TEST_URL)
		self.driver.set_window_size(1920, 1000)

		self.vars = {}


	def tearDown(self):
		driver = self.driver

		driver.close()
		driver.quit()


	@parameterized.expand([
			("Beispiel1",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "-0.1",
"inTextFlatnessMeasureBlack2": "-0.1",
"inTextFlatnessMeasureColor1": "-0.05",
"inTextFlatnessMeasureColor2": "-0.05",
"inTextThicknessMeasureBlack1": "3.9",
"inTextThicknessMeasureBlack2": "3.9",
"inTextThicknessMeasureBlack3": "3.9",
"inTextThicknessMeasureBlack4": "3.9",
"inTextThicknessMeasureColor1": "2",
"inTextThicknessMeasureColor2": "2",
"inTextThicknessMeasureColor3": "2",
"inTextThicknessMeasureColor4": "2",
				},
				{
"outTextFlatnessBlackMaximum": "-0.1",
"outTextFlatnessBlackMinimum": "-0.1",
"outTextFlatnessColorMaximum": "-0.05",
"outTextFlatnessColorMinimum": "-0.05",
"outTextThicknessAverageBlack": "3.9",
"outTextThicknessAverageColor": "2",
"outTextThicknessFinalBlack": "3.9",
"outTextThicknessFinalColor": "2",
				}
			),
			("Beispiel2",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "0.1",
"inTextFlatnessMeasureBlack2": "0.1",
"inTextFlatnessMeasureColor1": "0.05",
"inTextFlatnessMeasureColor2": "0.05",
"inTextThicknessMeasureBlack1": "3.9",
"inTextThicknessMeasureBlack2": "3.9",
"inTextThicknessMeasureBlack3": "3.9",
"inTextThicknessMeasureBlack4": "3.9",
"inTextThicknessMeasureColor1": "2",
"inTextThicknessMeasureColor2": "2",
"inTextThicknessMeasureColor3": "2",
"inTextThicknessMeasureColor4": "2",
				},
				{
"outTextFlatnessBlackMaximum": "0.1",
"outTextFlatnessBlackMinimum": "0.1",
"outTextFlatnessColorMaximum": "0.05",
"outTextFlatnessColorMinimum": "0.05",
"outTextThicknessAverageBlack": "3.9",
"outTextThicknessAverageColor": "2",
"outTextThicknessFinalBlack": "4",
"outTextThicknessFinalColor": "2.05",
				}
			),
			("Beispiel3",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "0.1",
"inTextFlatnessMeasureBlack2": "0.1",
"inTextFlatnessMeasureColor1": "-0.05",
"inTextFlatnessMeasureColor2": "-0.05",
"inTextThicknessMeasureBlack1": "3.9",
"inTextThicknessMeasureBlack2": "3.9",
"inTextThicknessMeasureBlack3": "3.9",
"inTextThicknessMeasureBlack4": "3.9",
"inTextThicknessMeasureColor1": "2",
"inTextThicknessMeasureColor2": "2",
"inTextThicknessMeasureColor3": "2",
"inTextThicknessMeasureColor4": "2",
				},
				{
"outTextFlatnessBlackMaximum": "0.1",
"outTextFlatnessBlackMinimum": "0.1",
"outTextFlatnessColorMaximum": "-0.05",
"outTextFlatnessColorMinimum": "-0.05",
"outTextThicknessAverageBlack": "3.9",
"outTextThicknessAverageColor": "2",
"outTextThicknessFinalBlack": "3.95",
"outTextThicknessFinalColor": "2",
				}
			),
			("Beispiel4",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "0.1",
"inTextFlatnessMeasureBlack2": "0.1",
"inTextFlatnessMeasureColor1": "-0.2",
"inTextFlatnessMeasureColor2": "-0.2",
"inTextThicknessMeasureBlack1": "3.9",
"inTextThicknessMeasureBlack2": "3.9",
"inTextThicknessMeasureBlack3": "3.9",
"inTextThicknessMeasureBlack4": "3.9",
"inTextThicknessMeasureColor1": "2",
"inTextThicknessMeasureColor2": "2",
"inTextThicknessMeasureColor3": "2",
"inTextThicknessMeasureColor4": "2",
				},
				{
"outTextFlatnessBlackMaximum": "0.1",
"outTextFlatnessBlackMinimum": "0.1",
"outTextFlatnessColorMaximum": "-0.2",
"outTextFlatnessColorMinimum": "-0.2",
"outTextThicknessAverageBlack": "3.9",
"outTextThicknessAverageColor": "2",
"outTextThicknessFinalBlack": "3.9",
"outTextThicknessFinalColor": "2",
				}
			),
			("Test1",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "0",
"inTextFlatnessMeasureBlack2": "0",
"inTextFlatnessMeasureColor1": "0",
"inTextFlatnessMeasureColor2": "0",
"inTextThicknessMeasureBlack1": "4",
"inTextThicknessMeasureBlack2": "4",
"inTextThicknessMeasureBlack3": "4",
"inTextThicknessMeasureBlack4": "4",
"inTextThicknessMeasureColor1": "4",
"inTextThicknessMeasureColor2": "4",
"inTextThicknessMeasureColor3": "4",
"inTextThicknessMeasureColor4": "4",
				},
				{
"outTextFlatnessBlackMaximum": "0",
"outTextFlatnessBlackMinimum": "0",
"outTextFlatnessColorMaximum": "0",
"outTextFlatnessColorMinimum": "0",
"outTextThicknessAverageBlack": "4",
"outTextThicknessAverageColor": "4",
"outTextThicknessFinalBlack": "4",
"outTextThicknessFinalColor": "4",
				}
			),
			("Test2",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "0.15",
"inTextFlatnessMeasureBlack2": "0.2",
"inTextFlatnessMeasureColor1": "-0.3",
"inTextFlatnessMeasureColor2": "-0.17",
"inTextThicknessMeasureBlack1": "2.1",
"inTextThicknessMeasureBlack2": "2.11",
"inTextThicknessMeasureBlack3": "2.01",
"inTextThicknessMeasureBlack4": "2.11",
"inTextThicknessMeasureColor1": "4",
"inTextThicknessMeasureColor2": "3.98",
"inTextThicknessMeasureColor3": "3.99",
"inTextThicknessMeasureColor4": "4",
				},
				{
"outTextFlatnessBlackMaximum": "0.2",
"outTextFlatnessBlackMinimum": "0.15",
"outTextFlatnessColorMaximum": "-0.17",
"outTextFlatnessColorMinimum": "-0.3",
"outTextThicknessAverageBlack": "2.0825",
"outTextThicknessAverageColor": "3.9925",
"outTextThicknessFinalBlack": "2.1125",
"outTextThicknessFinalColor": "3.9925",
				}
			),
			("Test3",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "-0.3",
"inTextFlatnessMeasureBlack2": "-0.17",
"inTextFlatnessMeasureColor1": "0.15",
"inTextFlatnessMeasureColor2": "0.2",
"inTextThicknessMeasureBlack1": "4",
"inTextThicknessMeasureBlack2": "3.98",
"inTextThicknessMeasureBlack3": "3.99",
"inTextThicknessMeasureBlack4": "4",
"inTextThicknessMeasureColor1": "2.1",
"inTextThicknessMeasureColor2": "2.11",
"inTextThicknessMeasureColor3": "2.01",
"inTextThicknessMeasureColor4": "2.11",
				},
				{
"outTextFlatnessBlackMaximum": "-0.17",
"outTextFlatnessBlackMinimum": "-0.3",
"outTextFlatnessColorMaximum": "0.2",
"outTextFlatnessColorMinimum": "0.15",
"outTextThicknessAverageBlack": "3.9925",
"outTextThicknessAverageColor": "2.0825",
"outTextThicknessFinalBlack": "3.9925",
"outTextThicknessFinalColor": "2.1125",
				}
			),
			("Test4",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "-0.5",
"inTextFlatnessMeasureBlack2": "-0.5",
"inTextFlatnessMeasureColor1": "0.2",
"inTextFlatnessMeasureColor2": "0.2",
"inTextThicknessMeasureBlack1": "4.0499",
"inTextThicknessMeasureBlack2": "4.0499",
"inTextThicknessMeasureBlack3": "4.0499",
"inTextThicknessMeasureBlack4": "4.0499",
"inTextThicknessMeasureColor1": "4.0499",
"inTextThicknessMeasureColor2": "4.0499",
"inTextThicknessMeasureColor3": "4.0499",
"inTextThicknessMeasureColor4": "4.0499",
				},
				{
"outTextFlatnessBlackMaximum": "-0.5",
"outTextFlatnessBlackMinimum": "-0.5",
"outTextFlatnessColorMaximum": "0.2",
"outTextFlatnessColorMinimum": "0.2",
"outTextThicknessAverageBlack": "4.0499",
"outTextThicknessAverageColor": "4.0499",
"outTextThicknessFinalBlack": "4.0499",
"outTextThicknessFinalColor": "4.0499",
				}
			),
			("Test5",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "0.2",
"inTextFlatnessMeasureBlack2": "0.2",
"inTextFlatnessMeasureColor1": "0.2",
"inTextFlatnessMeasureColor2": "0.2",
"inTextThicknessMeasureBlack1": "3.8499",
"inTextThicknessMeasureBlack2": "3.8499",
"inTextThicknessMeasureBlack3": "3.8499",
"inTextThicknessMeasureBlack4": "3.8499",
"inTextThicknessMeasureColor1": "3.8499",
"inTextThicknessMeasureColor2": "3.8499",
"inTextThicknessMeasureColor3": "3.8499",
"inTextThicknessMeasureColor4": "3.8499",
				},
				{
"outTextFlatnessBlackMaximum": "0.2",
"outTextFlatnessBlackMinimum": "0.2",
"outTextFlatnessColorMaximum": "0.2",
"outTextFlatnessColorMinimum": "0.2",
"outTextThicknessAverageBlack": "3.8499",
"outTextThicknessAverageColor": "3.8499",
"outTextThicknessFinalBlack": "4.0499",
"outTextThicknessFinalColor": "4.0499",
				}
			),
			("Test6",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "-0.5",
"inTextFlatnessMeasureBlack2": "-0.5",
"inTextFlatnessMeasureColor1": "-0.5",
"inTextFlatnessMeasureColor2": "-0.5",
"inTextThicknessMeasureBlack1": "4.0499",
"inTextThicknessMeasureBlack2": "4.0499",
"inTextThicknessMeasureBlack3": "4.0499",
"inTextThicknessMeasureBlack4": "4.0499",
"inTextThicknessMeasureColor1": "4.0499",
"inTextThicknessMeasureColor2": "4.0499",
"inTextThicknessMeasureColor3": "4.0499",
"inTextThicknessMeasureColor4": "4.0499",
				},
				{
"outTextFlatnessBlackMaximum": "-0.5",
"outTextFlatnessBlackMinimum": "-0.5",
"outTextFlatnessColorMaximum": "-0.5",
"outTextFlatnessColorMinimum": "-0.5",
"outTextThicknessAverageBlack": "4.0499",
"outTextThicknessAverageColor": "4.0499",
"outTextThicknessFinalBlack": "4.0499",
"outTextThicknessFinalColor": "4.0499",
				}
			),
			("Test7",
				{
"inChkLARCBlack": True,
"inChkLARCColor": True,
"inChkVisual": True,
				},
				{
"inTextFlatnessMeasureBlack1": "0",
"inTextFlatnessMeasureBlack2": "0",
"inTextFlatnessMeasureColor1": "0",
"inTextFlatnessMeasureColor2": "0",
"inTextThicknessMeasureBlack1": "1",
"inTextThicknessMeasureBlack2": "1",
"inTextThicknessMeasureBlack3": "1",
"inTextThicknessMeasureBlack4": "1",
"inTextThicknessMeasureColor1": "2",
"inTextThicknessMeasureColor2": "2",
"inTextThicknessMeasureColor3": "2",
"inTextThicknessMeasureColor4": "2",
				},
				{
"outTextFlatnessBlackMaximum": "0",
"outTextFlatnessBlackMinimum": "0",
"outTextFlatnessColorMaximum": "0",
"outTextFlatnessColorMinimum": "0",
"outTextThicknessAverageBlack": "1",
"outTextThicknessAverageColor": "2",
"outTextThicknessFinalBlack": "1",
"outTextThicknessFinalColor": "2",
				}
			),
	])
	def test_correct(self, name, clicks, inputs, expectations):
		driver = self.driver

		for (id, value) in clicks.items():
			driver.find_element(By.ID, id).click()

		for (id, value) in inputs.items():
			driver.find_element(By.ID, id).send_keys(value)

		for (id, value) in expectations.items():
			self.assertEqual(driver.find_element(By.ID, id).get_attribute("value"), value)

		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-heading").text, "Der Schläger ist korrekt.")
		self.assertEqual(driver.find_element(By.ID, "result-body").text, "Beide Beläge sowie das Holz haben alle Tests bestanden.")



# run all tests (funcations that start with "test_")
if __name__ == "__main__":

	unittest.main()

# eof
