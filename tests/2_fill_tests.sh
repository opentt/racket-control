echo "Fill tests with test values."
echo

CONTAINER_FMPP=fmpp
IMAGE_FMPP=ekleinod/$CONTAINER_FMPP
WORKDIR_FMPP=/local

docker run \
	--rm \
	--name $CONTAINER_FMPP \
	--volume "${PWD}:$WORKDIR_FMPP" \
	$IMAGE_FMPP \
		--data="measurements:json($WORKDIR_FMPP/TestData/Measurements.json)" \
		--source-root="$WORKDIR_FMPP/selenium_test_templates" \
		--source-encoding=UTF-8 \
		--output-root="$WORKDIR_FMPP/selenium_tests" \
		--output-encoding=UTF-8 \
		--modes="ignore(*.ftl)"
