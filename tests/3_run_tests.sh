# This file is part of Open-TT Racket Control.
# Ekkart Kleinod (https://www.edgesoft.de/)

VERBOSE=""
DEFAULT_TEST_MODULE="selenium_tests"

# help
function showHelp() {
	echo "NAME"
	echo -e "\trun_tests.sh - run selenium tests"
	echo
	echo "SYNOPSIS"
	echo -e "\trun_tests.sh [-i TEST-MODULE] [-v]"
	echo
	echo "DESCRIPTION"
	echo -e "\tThis script runs selenium tests."
	echo
	echo -e "\t-i\ttest module, default: $DEFAULT_TEST_MODULE"
	echo -e "\t-v\tverbose output"
	echo -e "\t-h\tshow this help"
	echo
}

testmodule=$DEFAULT_TEST_MODULE

# parameters
while getopts hi:v option
do
	case "${option}"
		in
			i)
				testmodule=${OPTARG}
				;;
			v)
				VERBOSE="-v"
				;;
			h | *)
				showHelp
				exit 0
				;;
	esac
done

echo "Runs selenium tests of module '$testmodule'."
echo

CONTAINER_PYTHON_SELENIUM=python-selenium
IMAGE_PYTHON_SELENIUM=ekleinod/$CONTAINER_PYTHON_SELENIUM
WORKDIR_PYTHON=/usr/src/myapp

# python call
# calling "python -m unittest module" does not work, therefore the cd approach is used
cd $testmodule
docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINER_PYTHON_SELENIUM \
	--volume "${PWD}":$WORKDIR_PYTHON \
	--workdir $WORKDIR_PYTHON \
	--network "host" \
	$IMAGE_PYTHON_SELENIUM \
		-m unittest \
		$VERBOSE

# eof
