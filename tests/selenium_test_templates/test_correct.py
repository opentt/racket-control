import unittest

from parameterized import parameterized

from selenium.webdriver.common.by import By

from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.chrome.options import Options


TEST_URL = "https://www.opentt.de/racket-control/"
# TEST_URL = "http://localhost:8000/"

class TestUI(unittest.TestCase):

	def setUp(self):

		options = Options()
		options.add_argument("--headless")
		options.add_argument("--no-sandbox")

		self.driver = WebDriver(options=options)
		self.driver.get(TEST_URL)
		self.driver.set_window_size(1920, 1000)

		self.vars = {}


	def tearDown(self):
		driver = self.driver

		driver.close()
		driver.quit()

<#macro print_value id value >"${id}": <#if value?is_boolean >${value?string("True", "False")}<#else>"${value}"</#if>,
</#macro>
<#assign IN = "in" />
<#assign OUT = "out" />
<#assign CHK = "Chk" />
<#assign TEXT = "Text" />

	@parameterized.expand([
		<#list measurements.test_correct as test_id, test_values>
			("${test_id}",
				{
					<#list test_values?keys?filter(key -> key?starts_with(IN + CHK)) as id>
						<@print_value id test_values[id] />
					</#list>
				},
				{
					<#list test_values?keys?filter(key -> key?starts_with(IN + TEXT)) as id>
						<@print_value id test_values[id] />
					</#list>
				},
				{
					<#list test_values?keys?filter(key -> key?starts_with(OUT + TEXT)) as id>
						<@print_value id test_values[id] />
					</#list>
				}
			),
		</#list>
	])
	def test_correct(self, name, clicks, inputs, expectations):
		driver = self.driver

		for (id, value) in clicks.items():
			driver.find_element(By.ID, id).click()

		for (id, value) in inputs.items():
			driver.find_element(By.ID, id).send_keys(value)

		for (id, value) in expectations.items():
			self.assertEqual(driver.find_element(By.ID, id).get_attribute("value"), value)

		self.assertEqual(driver.find_element(By.CSS_SELECTOR, "#result-heading").text, "Der Schläger ist korrekt.")
		self.assertEqual(driver.find_element(By.ID, "result-body").text, "Beide Beläge sowie das Holz haben alle Tests bestanden.")



# run all tests (funcations that start with "test_")
if __name__ == "__main__":

	unittest.main()

# eof
