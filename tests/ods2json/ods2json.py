import argparse
import json
import logging
import os
import pyexcel_ods3
import pyexcel
import sys


# load ods
def load_ods_as_book(input_file):

	if os.path.isfile(input_file):

		logging.info("Loading '%s'.", input_file)

		content = pyexcel.get_book(file_name=input_file)

	else:
		raise FileNotFoundError("File '{}' does not exist or is not a file.".format(input_file))

	return content


# transpose sheets
def transpose_book(ods_book):

	for sheet_name in ods_book.sheet_names():
		ods_book[sheet_name].transpose()

	return ods_book


# name rows by column
def name_rows_by_column_book(ods_book):

	for sheet_name in ods_book.sheet_names():
		ods_book[sheet_name].name_rows_by_column(0)

	return ods_book


# convert excel book to json
def book_to_json(ods_book):

	return ods_book.bookdict

# flatten test cases from book
def flatten_book(ods_book):


	flattened = {}
	for sheet_name in filter(lambda name: name.startswith("test_"), ods_book.sheet_names()):

		flattened[sheet_name] = {}

		sheet_dict = ods_book[sheet_name].dict
		for i in range(len(sheet_dict["id"])):
			data_id = sheet_dict["id"][i]
			flattened[sheet_name][data_id] = {}
			for (id, values) in sheet_dict.items():
				if (
					(id != "id") and
					(id != "") and
					(id != "-1")
				):
					flattened[sheet_name][data_id][id] = values[i]

	return flattened


# save json file
def save_json_file(json_data, filename):

	logging.info("Saving to '%s'", filename)
	# os.makedirs(os.path.dirname(filename), exist_ok=True)
	with open(filename, 'w', encoding='utf-8') as outfile:
		json.dump(json_data, outfile, sort_keys = True, indent = 4, default = str)

	return


# init args parser, read and save sources
if __name__ == "__main__":

	logging.basicConfig(format='%(asctime)s | %(levelname)s | %(message)s', datefmt='%Y-%m-%d | %H:%M:%S', level=logging.WARNING)

	parser = argparse.ArgumentParser(description = "Convert csv to json.")
	parser.add_argument("-i", "--input", type = str, help = "input file", required = True)
	parser.add_argument("-o", "--output", type = str, help = "output file", required = True)
	parser.add_argument("-v", "--verbose", action='store_true', help = "verbose output", required = False)

	args = parser.parse_args()

	try:
		if (args.verbose):
			logging.getLogger().setLevel(logging.INFO)

		ods_book = load_ods_as_book(args.input)
		json_data = flatten_book(name_rows_by_column_book(ods_book))
		save_json_file(json_data, args.output)

	except Exception as e:
		exc_type, exc_obj, exc_tb = sys.exc_info()
		fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
		logging.error("%s in %s, line %d | %s", type(e).__name__, fname, exc_tb.tb_lineno, e)
