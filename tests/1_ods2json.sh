echo "Convert ods to json"
echo

CONTAINER_PYTHON=python
IMAGE_PYTHON=ekleinod/$CONTAINER_PYTHON
WORKDIR_PYTHON=/usr/src/myapp

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--interactive \
	--tty \
	--name $CONTAINER_PYTHON \
	--volume "${PWD}":$WORKDIR_PYTHON \
	--workdir $WORKDIR_PYTHON \
	$IMAGE_PYTHON \
		ods2json/ods2json.py \
			--input "TestData/Measurements.ods" \
			--output "TestData/Measurements.json" \
			--verbose

# eof
