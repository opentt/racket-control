#!/bin/bash
echo "Run server at http://localhost:8000"
echo "inspect with: docker exec --interactive --tty apache bash"
echo

docker-compose up --detach
